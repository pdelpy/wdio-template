const mainConfig = require("./wdio.conf").config;

const browserstackConfig = {
    ...mainConfig,
    capabilities: [{
        browserName: 'chrome',
        acceptInsecureCerts: true,
        'project': 'WebdriverIO',
        'browserstack.debug' : 'true',
        'browserstack.networkLogs' : 'true'
    }],
    services: ['browserstack'],
    user: process.env.BROWSERSTACK_USER,
    key: process.env.BROWSERSTACK_KEY,
    browserstackLocal: true
};

exports.config = browserstackConfig;