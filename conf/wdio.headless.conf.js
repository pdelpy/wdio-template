const mainConfig = require('./wdio.conf').config;

const headlessConfig = {
    ...mainConfig,
    capabilities: [
        {
            browserName: 'chrome',
            'goog:chromeOptions': {
                args: [
                    '--window-size=1440,900',
                    '--no-sandbox',
                    '--disable-infobars',
                    '--headless',
                    '--disable-gpu',
                ],
                prefs: {
                    'intl.accept_languages': 'en',
                },
            },
        },
    ],
    services: [],
};

exports.config = headlessConfig;
